from wkhtmltopdf import wkhtmltopdf
from bs4 import BeautifulSoup
from pdfrw import PdfReader, PdfWriter, IndirectPdfDict
import requests
import urlparse
import os


def getPages():
    startUrl = 'http://r4ds.had.co.nz/index.html'
    resp = requests.get(startUrl)
    soup = BeautifulSoup(resp.content, 'html.parser')
    links = map(lambda y: y.findAll('a')[0]['href'],
                filter(lambda x: x['data-level'].count('.') == 0,
                       soup.findAll('li', {'class': 'chapter'})))
    url = urlparse.urlparse(startUrl)
    return [url.scheme + "://" + url.hostname + "/" + link for link in links]


def renderPdfs(links):
    cwd = os.getcwd()
    fileNameToLink = {os.path.join(cwd, str(cnt) + ".pdf"): link for cnt,
                      link in zip(range(len(links)), links)}

    for k in fileNameToLink:
        wkhtmltopdf(fileNameToLink[k], k)

    # we can return relative paths for the down stream functions
    return sorted(map(os.path.basename, fileNameToLink.keys()),
                  cmp=lambda x, y: int(x[:x.find('.')]) - int(y[:y.find('.')]))


def mergePdfs(pdfs):

    writer = PdfWriter()
    for pdf in pdfs:
        writer.addpages(PdfReader(pdf).pages)
    writer.trailer.Info = IndirectPdfDict(
        Title="R For Data Science",
        Author='Hadley Wickham & Garett Grolemund',
        Subject='Data Science, Rlang'
    )
    writer.write(os.path.join(os.getcwd(), 'RForDataScience.pdf'))
    return pdfs


def delFiles(files):
    for file in files:
        os.remove(file)


def main():
    pipeline = [getPages, renderPdfs, mergePdfs, delFiles]
    currentData = None
    for f in pipeline:
        currentData = f(currentData) if currentData is not None else f()


if __name__ == "__main__":
    main()
