# README #

A python script to generate an offline PDF of R For Data Science by Garrett Grolemund and Hadley Wickham.
The book is available from http://r4ds.had.co.nz/index.html under the createive commons license.

### How do I get set up? ###

Install the [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html) binaries. This can be as simple as:
```sh
$ sudo apt-get install wkhtmltopdf
```

Create a python virtual environment or install the dependencies globally using 
```sh
$(sudo) pip -r requirements.txt
```
Run the script to generate the pdf.

```sh
python generate_book.py
```

### Dependencies ###
- wkhtmltopdf (binaries)
- beautifulsoup
- wkhtmltopdf (python package)
- pdfrw
- requests

### Who do I talk to? ###
* Message me on the canvas forum

